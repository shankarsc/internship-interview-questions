# Internship Interview Questions
*Please include explanation along with your answers.*

1. Please describe yourself using JSON (include your internship start/end date, your current location).
    ```
    {
        "personalDetails": [
            {
            "firstName": "Shankar", 
            "lastName": "Sathish Chandran", 
            "email": "shankar.sathish@gmail.com", 
            "location": "No. 13, Jalan Puteri 12/25, Bandar Puteri Puchong, 47100, Selangor"
            }
        ],

        "personalSummary": [
            "Highly motivated, ambitious, and teamwork-oriented Mechanical Engineering graduate with a passion for all things software-related.
            Presently, seeking an internship within the software engineering industry to develop current programming skills and take on new challenges
            in a professional environment. Aside from academics, active representation of state and national cricket team at junior and senior levels
            have enhanced the development of soft skills suitable for a professional working environment."
        ],

        "internshipDetails": [
            {
            "startDate": "5/10/2020",
            "endDate": "28/12/2020"
            }
        ],

        "universityEducation": [ 
            {
            "courseOfStudy": "Bachelor of Mechanical Engineering (Honours)",
            "university": "Monash University Malaysia",
            "graduationDate": "July 2020",
            "comments": ["Graduated with Second Class Honours (Div. A)", "Member of Monash University Elite Performer Scheme 2018/19/20"]
            }
        ],

        "preuEducation": [
            {
            "courseOfStudy": "Australian Matriculation",
            "university": "Sunway University",
            "graduationDate": "July 2020",
            "comments": ["Sunway University Sports Scholarship Recipient 2015"]
            }
        ],

        "formalExperience": [
            {
                "internship": [
                    {
                    "position": "Data Science Intern",
                    "company": "Metora Co.",
                    "period": "December 2019 - February 2020"
                    },
                    {
                    "position": "Pipeline Engineering Intern",
                    "company": "Berachah Group Sdn. Bhd.",
                    "period": "November 2018 - February 2019"
                    }
                ]
            }
        ],

        "technicalSkills": ["Python, C++, MATLAB, MySQL, MS SQL Server 2016, MongoDB"]
    }
    ```

2. Tell us about a newer (less than five years old) web technology you like and why?

> I am excited to see the capabilities of Solana play out in the future. Solana is a permissionless blockchain capable of sustaining throughput over 
50,000 transactions per second when running with GPUs. This technology uses delegated Proof-of-Stake protocol to secure the blockchain network with
Proof-of-History as the solution to scaling issues faced by older Layer 1 technologies. By using a high frequency Verifiable Delay Function, each node
within the Solana blockchain is able to generate timestamps locally with SHA-256 computations, improving overall network efficiency as broadcast of
timestamps across the network is not needed. I find this technology fascinating as it may be a potential solution to the scaling issues faced by
Bitcoin, Ethereum and other layer 1 networks. Solana may return better feedback from retail participants in terms of adoption due to their much higher
transactions per second of nearly 10,000x transactions per second relative to Bitcoin while remaining decentralised and scalable.


3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?

> One could declare the Array to be a Vector, where the size of the Vector could expand or shrink as needed to accommodate addition or removal of elements after Vector has been initialized. One could also specify an Array typically larger than the number of elements currently required. An issue with this approach is that if the number of elements reaches the maximum size of the Array, then a new array has to be allocated and each element has to be copied from the original array. This approach is computationally more expensive than the first approach.


4. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```


> The Big O-notation is commonly used to classify algorithms in terms of their run time or disk space requirements as the size of input grows. In this 
block of code, an array is initialised with an unknown maximum size and has integers inserted into the array via an outer and inner nested for-loop. The 
outer for-loop is triggered for its first iteration and the inner nested for-loop is looped through until the number of iterations equals the predefined 
maximum size. The inner nested for-loop is exited and the outer for-loop begins its second iteration. This cycle repeats until the number of iterations 
in the outer for-loop equals the predefined maximum size, causing the entire loop to stop running. In this case, the algorithm performance is 
proportional to the square of the size of the input data, resulting in a big-O notation of O(N^2).


5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation.

> A glaring issue with the page is there are too many cryptocurrencies being displayed, resulting in excessive number of HTTP requests. The browser 
requests for JavaScript, CSS, and image files for all 500 cryptocurrencies simultaneously which can slow down the page load speed significantly. The 
large number of cryptocurrencies displayed on a single page also would result in an equally large number of external scripts to query the data for all 
cryptocurrencies, thus resulting in a large number of network roundtrips before the page can be rendered.


> One solution is to reduce the number of cryptocurrencies displayed on the page to a reasonable number (<=100), which would reduce the number of files to load and speed up the page load time. Another solution is to remove/inline any render-blocking JavaScripts necessary to render page content to avoid 
extra network requests. Scripts that are unnecessary for the initial page render should be deferred or made asynchronous until after the original render 
is complete. Doing so may speed up the page load time. Another potential solution is to optimise the page’s CSS in order to improve performance. One 
could inline CSS where necessary, or combine multiple CSS files into a single CSS file if possible.

6. In Javascript, What is a "closure"? How does JS Closure works?

> A closure is a combination of the function and the lexical environment within which the function was declared, wherein variables declared within an 
outer function’s scope is accessed from an inner function. A lexical environment consists of any local variables that were in-scope at the time the 
closure was created. A closure in Javascript can be explained by the following block of code:

    function makeFunc() {
        var name = 'Shankar';
        function displayName() {
            console.log(name);
            }
        return displayName;
        }

        var myFunc = makeFunc();
        myFunc();


> The variable name and inner function displayName() is defined within the function makeFunc(). When makeFunc() is invoked, the displayName() makes use of variable name to display its contents without requiring said variable as an input argument. The reason for this is that both variable name and 
displayName() function instances are defined within the same lexical environment of makeFunc(). The variable name remains available for access and 
‘Shankar’ is passed successfully into the terminal via console.log().

7. In Javascript, what is the difference between var, let, and const. When should I use them?

> Variable defined with var is able to be globally or function-scoped and re-declared and updated within the same scope. Variable declared with let is 
block-scoped and can be updated, but not re-declared in the same scope. If the same variables are defined in different scopes, re-declaration is 
allowed. Variable declared with const can only be accessed within the block they were declared in (block-scoped) but is not able to be re-updated or 
re-declared within said block.


> Const should be used as an identifier unless the value of the identifier is to be changed. Let is to be used when the value of an identifier is to be 
changed such as within a for-loop or if-loop. The introduction of const and let has led to the usage of var to be redundant, leading to var not needing 
to be used in the future.


8. Share with us one book that has changed your perspective in life. How did it change your life?

> David Graeber’s Bullshit Jobs is one book which has altered my perspective on how pointless certain jobs are and exist solely to keep a large percentage of the population working all of the time. A brief summary of the book is as follows. Following on from his article titled ‘On The Phenomenon of Bullshit Jobs’ published in 2013 wherein he states that in the year 1930, John Maynard Keynes predicted that by the year 2000, technology would have 
advanced productivity sufficiently enough to achieve 15-hour working weeks in the United States and Great Britain. Yet, this has not taken effect and 
Graeber goes onto challenge the idea that Keynes did not account for the massive increase in consumerism by arguing that most of the jobs in existence 
today have little to do with the production and distribution of consumer products as those jobs have been automated. Rather than allowing the working 
population to pursue their own projects due to a massive reduction in working hours, whole new industries such as telemarketing and expansion of 
industries relating to corporate law, Human Resources, academic and health administration have come into effect. Jobs in which Graeber defines as 
pointless. After several revisions on the definition of said jobs within the book, Graeber arrives to a definition of:


> “A bullshit job is a form of paid employment that is so completely pointless, unnecessary or pernicious that not even the employee cannot justify its 
existence even though, as part of the conditions of employment, the employee feels obligated to pretend that is not the case.”


> Graeber classifies these pointless jobs into 5 such types and argues that most of these jobs exist within the private sector despite the claim that 
market inefficiencies would eliminate these inefficiencies. Graeber then goes onto describe how society in itself has entrenched the idea of work ethic 
being a determinant of self-worth and the belief that suffering justified through the toil of working is noble even though the jobs are pointless. This 
cycle coupled with lack of awareness has resulted in society discriminating on jobs that are largely fulfilling. Ultimately, this book has made clear to 
me that I would rather spend my time working on something I believe to be useful and beneficial, rather than dedicate my time in a sector which is 
innately pointless and would not make a difference to the world if it were to disappear tomorrow. While this may narrow down future job opportunities 
for me in certain sectors, I stand firm with my decision and in a way, this has led to me having a clearer picture of what contributions I would want to 
achieve during my working years.


9. What is the thing you believe in that is true, that most people disagree with?

> Personally, I do believe that humankind are the only forms of intelligent life present in the current universe. While the universe is of unfathomable 
size and scale, the sheer probability of life to exist, let alone evolve into an intelligent form of life is very small. For life to exist, a planet 
must be within a so-called habitable zone which is a certain range of distance from a star. While there are many billions of stars in the universe and 
many more planets orbiting these stars, for life to evolve into an intelligent one, meaning it is capable of independent thought with the ability to 
communicate, is incredibly tiny. Earth is 13.7 billion years old yet primitive humans came into existence only 2 million years ago. The film industry 
has done a wonderful job in sparking the imaginations of viewers all around the world with the idea that there are other more intelligent lifeforms in 
the universe looking to exterminate other less intelligent forms of life but ultimately, it has been drastically dramatised. Only once we are able to 
receive a clear, coherent communication signal from life beyond Earth, then I may start to believe intelligent life exists outside of Earth.


10. What are your thoughts on the subject of Arts and Humanities?


> I believe the subject plays an important role in understanding how humankind have come to possess unique ideas, practices, and cultures depending on the time period or location of said group of humans. It is a fascinating subject as it allows a deeper insight into how we have evolved thus far, and how we may yet evolve based on studies that would be covered in Arts and Humanities. This subjects deserves more recognition for its role in educating society. From time to time, I enjoy exploring areas of study within Arts and Humanities such as archaeology, history, visual art, and music.

---
# Simple Coding Assessment

Build a Cryptocurrency Market dashboard showing market data.

requirement:
1. using the API endpoints found in https://www.coingecko.com/en/api, build a cryptocurrency market dashboard page.
2. The page should be able to list at least 20 coins.
3. The page should show price, volume, name, symbol of the coin.
4. The page should show the graph of 7 days data, using the **sparkline** returned from api. For example sparkline data can be obtained using [https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true](https://api.coingecko.com/api/v3/coins/bitcoin?sparkline=true) or [https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true](https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&sparkline=true)
5. The page should allow user to "favorite" coins.
6. build another page "favorite", to only show the favorite coins.
5. **(bonus)** The page should allow user to switch currency for ("usd", "myr", "btc"). The price and volume should display the number in the currency selected.
6. **(bonus)** Host this on a website, or a mobile app.
7. We will schedule a video call with you should we decide to proceed with your interview.
8. You must be able to demo your submission to us.

---
# Submission instruction

1. Fork this repo.
2. Creates a Merge Request in this repo after completion.
